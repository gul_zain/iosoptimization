//
//  DangerViewController.h
//  DangerObjectDetectSample
//
//  Created by Dmytro Hrebeniuk on 1/17/19.
//  Copyright © 2019 SquareDetect. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DangerViewController : UIViewController

@end

NS_ASSUME_NONNULL_END

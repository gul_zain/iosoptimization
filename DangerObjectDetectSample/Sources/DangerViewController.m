//
//  DangerViewController.m
//  DangerObjectDetectSample
//
//  Created by Dmytro Hrebeniuk on 1/17/19.
//  Copyright © 2019 SquareDetect. All rights reserved.
//

#import "DangerViewController.h"
#import <DangerObjectDetect/DangerObjectDetect.h>

@interface DangerViewController ()

@property (strong, nonatomic) IBOutlet UITextView *timeLog;

@property (strong) DangerObjectsDetector *dangerObjectsDetector;

@end

static NSMutableArray* sImagesResults;

void callbackFromSwift(void* pointer) {
    UIImage *image = (__bridge UIImage *)pointer;
    [sImagesResults addObject:image];
}

@implementation DangerViewController

- (UIImage *)scaleImage:(UIImage *)image toScale:(CGFloat)scale {
    CGSize newSize = CGSizeMake(image.size.width*scale, image.size.height*scale);
    
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.5);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resizedImage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sImagesResults = [NSMutableArray array];
    
    self.dangerObjectsDetector = [[DangerObjectsDetector alloc] init];
    
    NSURL *directoryURL = [[NSBundle mainBundle] URLForResource:@"TestImages" withExtension:nil];
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: directoryURL.path error: nil];
    
    NSMutableArray *grayFiles = [NSMutableArray array];
    for (NSString *file in files) {
        if ([file containsString: @"gray"]) {
            [grayFiles addObject:file];
        }
    }
    
    NSMutableArray *rgbImages = [NSMutableArray array];
    NSMutableArray *grayImages = [NSMutableArray array];
    
    for (NSString *grayFile in grayFiles) {
        NSString *rgbFile = [grayFile stringByReplacingOccurrencesOfString:@"gray" withString:@"rgb"];
        NSURL *grayImageURL = [directoryURL URLByAppendingPathComponent:grayFile];
        NSURL *rgbImageURL = [directoryURL URLByAppendingPathComponent:rgbFile];
        
        UIImage *rgbImage = [self scaleImage:[UIImage imageWithContentsOfFile: rgbImageURL.path] toScale:0.5];
        UIImage *grayImage = [UIImage imageWithContentsOfFile: grayImageURL.path];
        
        [rgbImages addObject:rgbImage];
        [grayImages addObject:grayImage];
    }
    
    [DangerObjectsDetector createEngineWrapper:callbackFromSwift];
    
    NSTimeInterval timeStampStart = [[NSDate date] timeIntervalSince1970];

    UIImage *dangerImage = [UIImage imageNamed:@"DangerImage"];
    
    [DangerObjectsDetector startWrapper];
    NSUInteger cnt = rgbImages.count;
    for (NSUInteger idx=0;idx<cnt;idx++) {
        [DangerObjectsDetector requestJobWrapper:rgbImages[idx] inGrayImage:grayImages[idx] dangerImage:dangerImage offsetSize:CGSizeZero];
    }
    
    [DangerObjectsDetector waitFinishedWrapper];
    
    NSTimeInterval timeStampEnd = [[NSDate date] timeIntervalSince1970];
    NSUInteger time = (timeStampEnd - timeStampStart)/cnt;
    NSString *timeStr = [NSString stringWithFormat:@"average time per image: %@ ms", @(time)];
    self.timeLog.text = timeStr;
    
    [DangerObjectsDetector destroyWrapper];
}

@end
